/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package connector;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author GreenSoft Eng
 */
public class DBconnectMySql {
    
    public static String jdbcUrl = "jdbc:mysql://localhost/greensoft";
    public static String timeZone = "&useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=Europe/Moscow";
    public static String serverIdentity = "?useSSL=false";
    
    private Connection conn;
    
    public DBconnectMySql( ){
        try {
            
            Class.forName("com.mysql.cj.jdbc.Driver");
            System.out.println("Driver O.K.");
            
            //Connection conn = DriverManager.getConnection(url, user, passwd);
            conn = DriverManager.getConnection(jdbcUrl+serverIdentity+timeZone,"dipita25","flature25");
            System.out.println("Connexion effective !");
            
            
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    public Connection connection(){
        try {
            
            Class.forName("com.mysql.cj.jdbc.Driver");
            System.out.println("Driver O.K.");
            
            //Connection conn = DriverManager.getConnection(url, user, passwd);
            conn = DriverManager.getConnection(jdbcUrl+serverIdentity+timeZone,"dipita25","flature25");
            System.out.println("Connexion effective !");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return conn;
    }
    
    //il etait imperatif d'ajouter("throws SQLException") à cette methode pr utiliser ResulSet
    public ResultSet getPatient(int id)throws SQLException{
        //Création d'un objet Statement
        Statement state = this.getConn().createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,ResultSet.CONCUR_UPDATABLE);
        //L'objet ResultSet contient le résultat de la requête SQL
        ResultSet result = state.executeQuery("SELECT * FROM patient WHERE id ="+ id );
        
        return result;
    }
    
    public Connection getConn(){
        return this.conn;
    }
    public void setConn(Connection conn){
        this.conn = conn;
    }
}
