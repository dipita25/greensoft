/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package connector;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author GreenSoft Eng
 */
public class DBconnectPostgreSql {
    
    public static String jdbcUrl = "jdbc:mysql://localhost/greensoft";
    public static String timeZone = "&useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=Europe/Moscow";
    public static String serverIdentity = "?useSSL=false";
    
    private Connection conn;
    
    public DBconnectPostgreSql( ){
        try {
            
            Class.forName("org.postgresql.Driver");
            System.out.println("Driver O.K.");
            
            String url = "jdbc:postgresql://localhost:5432/greensoft";
            String user = "postgres";
            String passwd = "flature25";

            conn = DriverManager.getConnection(url, user, passwd);
            System.out.println("Connexion effective !"); 
            
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    public Connection connection(){
        try {
            
            Class.forName("org.postgresql.Driver");
            System.out.println("Driver O.K.");
            
            String url = "jdbc:postgresql://localhost:5432/greensoft";
            String user = "postgres";
            String passwd = "flature25";

            this.conn = DriverManager.getConnection(url, user, passwd);
            System.out.println("Connexion effective !"); 
            
        } catch (Exception e) {
            e.printStackTrace();
        }
        return this.conn;
    }
//    
    //il etait imperatif d'ajouter("throws SQLException") à cette methode pr utiliser ResulSet
    public ResultSet getPatient(int id)throws SQLException{
        //Création d'un objet Statement
        Statement state = this.getConn().createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,ResultSet.CONCUR_UPDATABLE);
        //L'objet ResultSet contient le résultat de la requête SQL
        ResultSet result = state.executeQuery("SELECT * FROM Patient WHERE id_patient ="+ id );
        result.next();
        System.out.println("le nom du patient est : " + result.getString("nom"));
        
        return result;
    }
//    
    public Connection getConn(){
        return this.conn;
    }
    public void setConn(Connection conn){
        this.conn = conn;
    }
}
