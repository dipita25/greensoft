/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package corengine;

/**
 *
 * @author GreenSoft Eng
 */
public class Specialite_Medecin {
    private int id_specialite;
    private String numero_ordre;

    public Specialite_Medecin(int id_specialite, String numero_ordre) {
        this.id_specialite = id_specialite;
        this.numero_ordre = numero_ordre;
    }

    public int getId_specialite() {
        return id_specialite;
    }

    public void setId_specialite(int id_specialite) {
        this.id_specialite = id_specialite;
    }

    public String getNumero_ordre() {
        return numero_ordre;
    }

    public void setNumero_ordre(String numero_ordre) {
        this.numero_ordre = numero_ordre;
    }
    
    
    
}
