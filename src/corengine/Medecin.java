/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package corengine;

/**
 *
 * @author GreenSoft Eng
 */
public class Medecin extends Personne{
    private String numero_ordre;
    private String password ;
    private String centre_medical ;

    public String getCentre_medical() {
        return centre_medical;
    }

    public void setCentre_medical(String centre_medical) {
        this.centre_medical = centre_medical;
    }
    
    public String getNumero_ordre() {
        return numero_ordre;
    }

    public void setNumero_ordre(String numero_ordre) {
        this.numero_ordre = numero_ordre;
    }
    
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
    
    public Medecin(String nom, String prenom, String telephone, String pays, String sexe, String statut_matrimonial, String nombre_enfant, String email,String password,String numero_ordre,String centre_medical){
        super(nom,prenom,telephone,pays,sexe,email);
        this.password = password;
        this.numero_ordre = numero_ordre;
        this.centre_medical = centre_medical;
    }
}
