/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package corengine;

/**
 *
 * @author GreenSoft Eng
 */
public class Consultation {
    
    private String id_consultation;
    private String id_type_consultation;

    public Consultation(String id_type_consultation) {
        this.id_type_consultation = id_type_consultation;
    }

    public String getId_consultation() {
        return id_consultation;
    }

    public void setId_consultation(String id_consultation) {
        this.id_consultation = id_consultation;
    }

    public String getId_type_consultation() {
        return id_type_consultation;
    }

    public void setId_type_consultation(String id_type_consultation) {
        this.id_type_consultation = id_type_consultation;
    }
    
    
    
}
