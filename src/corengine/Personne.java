/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package corengine;

/**
 *
 * @author GreenSoft Eng
 */
public abstract class Personne {
    
    protected String nom;
    protected String prenom;
    protected String telephone;
    protected String pays;
    protected String sexe;
    protected String email;

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getPays() {
        return pays;
    }

    public void setPays(String pays) {
        this.pays = pays;
    }

    public String getSexe() {
        return sexe;
    }

    public void setSexe(String sexe) {
        this.sexe = sexe;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Personne(String nom, String prenom, String telephone, String pays, String sexe, String email) {
        this.nom = nom;
        this.prenom = prenom;
        this.telephone = telephone;
        this.pays = pays;
        this.sexe = sexe;
        this.email = email;
    }
    
    
    
}
