/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package corengine;

/**
 *
 * @author GreenSoft Eng
 */
public class Patient_Medecin {
    
    private int id_patient;
    private String numero_ordre;

    public Patient_Medecin(int id_patient, String numero_ordre) {
        this.id_patient = id_patient;
        this.numero_ordre = numero_ordre;
    }

    public int getId_patient() {
        return id_patient;
    }

    public void setId_patient(int id_patient) {
        this.id_patient = id_patient;
    }

    public String getNumero_ordre() {
        return numero_ordre;
    }

    public void setNumero_ordre(String numero_ordre) {
        this.numero_ordre = numero_ordre;
    }
    
    
}
