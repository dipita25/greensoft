/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package corengine;


/**
 *
 * @author jonathan
 */
public class Patient extends Personne{
    
    
    protected String statut_matrimonial;
    protected int nombre_enfant;
    protected int glycemie;
    protected String seropositivite;
    protected String groupe_sanguin;
    protected int taille;
    protected int masse_corporelle;
    
    public Patient(String nom, String prenom, String telephone, String pays, String sexe, String statut_matrimonial, int nombre_enfant, String email,String groupe_sanguin,String seropositivite,int glycemie,int taille,int masse_corporelle){
        super(nom,prenom,telephone,pays,sexe,email);
        this.statut_matrimonial = statut_matrimonial;
        this.nombre_enfant = nombre_enfant;
        this.glycemie = glycemie;
        this.seropositivite = seropositivite;
        this.seropositivite = groupe_sanguin;
        this.taille = taille;
        this.masse_corporelle = masse_corporelle;
        this.groupe_sanguin = groupe_sanguin;
    }

    public String getStatut_matrimonial() {
        return statut_matrimonial;
    }

    public void setStatut_matrimonial(String statut_matrimonial) {
        this.statut_matrimonial = statut_matrimonial;
    }

    public int getNombre_enfant() {
        return nombre_enfant;
    }

    public void setNombre_enfant(int nombre_enfant) {
        this.nombre_enfant = nombre_enfant;
    }

    public int getGlycemie() {
        return glycemie;
    }

    public void setGlycemie(int glycemie) {
        this.glycemie = glycemie;
    }

    public String getSeropositive() {
        return seropositivite;
    }

    public void setSeropositive(String seropositivite) {
        this.seropositivite = seropositivite;
    }

    public String getGroupe_sanguin() {
        return groupe_sanguin;
    }

    public void setGroupe_sanguin(String groupe_sanguin) {
        this.groupe_sanguin = groupe_sanguin;
    }

    public int getTaille() {
        return taille;
    }

    public void setTaille(int taille) {
        this.taille = taille;
    }

    public int getMasse_corporelle() {
        return masse_corporelle;
    }

    public void setMasse_corporelle(int masse_corporelle) {
        this.masse_corporelle = masse_corporelle;
    }
    
    
}
