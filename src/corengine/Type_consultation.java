/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package corengine;

/**
 *
 * @author GreenSoft Eng
 */
public class Type_consultation {
    
    private String id_type_consultation;
    private String description_type_consultation;

    public Type_consultation(String description_type_consultation) {
        this.description_type_consultation = description_type_consultation;
    }

    public String getId_type_consultation() {
        return id_type_consultation;
    }

    public void setId_type_consultation(String id_type_consultation) {
        this.id_type_consultation = id_type_consultation;
    }

    public String getDescription_type_consultation() {
        return description_type_consultation;
    }

    public void setDescription_type_consultation(String description_type_consultation) {
        this.description_type_consultation = description_type_consultation;
    }
    
    
    
}
