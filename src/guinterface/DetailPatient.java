/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package guinterface;

import java.awt.Component;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 *
 * @author jonathan
 */
public class DetailPatient extends javax.swing.JFrame implements ActionListener{
    
    private JPanel panel_info_gene;
    private JPanel panel_adresse;
    private JPanel panel_civilite;
    private JPanel panel_sante;
    private JPanel panel_bouton;

    private JLabel label_nom_patient;
    private JLabel label_nom_patient_txt;
    private JLabel label_prenom;
    private JLabel label_prenom_txt;
    private JLabel label_age;
    private JLabel label_age_txt;
    private JLabel label_sexe;
    private JLabel label_sexe_txt;
    private JLabel label_nationalite;
    private JLabel label_nationalite_txt;
    private JLabel label_taille;
    private JLabel label_taille_txt;
    private JLabel label_pays;
    private JLabel label_pays_txt;
    private JLabel label_ville;
    private JLabel label_ville_txt;
    private JLabel label_numero_rue;
    private JLabel label_numero_rue_txt;
    private JLabel label_telephone_mobile;
    private JLabel label_telephone_mobile_txt;
    private JLabel label_telephone_domicile;
    private JLabel label_telephone_domicile_txt;
    private JLabel label_statut_matrimonial;
    private JLabel label_statut_matrimonial_txt;
    private JLabel label_nombre_enfants;
    private JLabel label_nombre_enfants_txt;
    private JLabel label_profession;
    private JLabel label_profession_txt;
    private JLabel label_groupe_sanguin;
    private JLabel label_groupe_sanguin_txt;
    private JLabel label_substances_allergenes;
    private JLabel label_substances_allergenes_txt;
    private JLabel label_masse_corporelle;
    private JLabel label_masse_corporelle_txt;
    private JLabel label_statut_serologique;
    private JLabel label_statut_serologique_txt;
    private JLabel label_glycemie;
    private JLabel label_glycemie_txt;
    private JButton bouton_close;
    private JButton bouton_consultations;
    /**
     * Creates new form fenetre1
     */
    public DetailPatient() {
        //initComponents();
        
        panel_info_gene = new JPanel();
        panel_adresse = new JPanel();
        panel_civilite = new JPanel();
        panel_sante = new JPanel();
        panel_bouton = new JPanel();
        
        label_nom_patient = new JLabel("nom  du patient");
        label_nom_patient_txt = new JLabel("label_nom_patient_txt");
        label_prenom = new JLabel("prenom du patient");
        label_prenom_txt = new JLabel("label_prenom_txt");
        label_age = new JLabel("age");
        label_age_txt = new JLabel("label_age_txt");
        label_sexe = new JLabel("sexe");
        label_sexe_txt = new JLabel("label_sexe_txt");
        label_nationalite = new JLabel("nationalité");
        label_nationalite_txt = new JLabel("label_nationalite_txt");
        label_taille = new JLabel("taille");
        label_taille_txt = new JLabel("label_taille_txt");
        label_pays = new JLabel("pays");
        label_pays_txt = new JLabel("label_pays_txt");
        label_ville = new JLabel("ville");
        label_ville_txt = new JLabel("label_ville_txt");
        label_numero_rue = new JLabel("numéro de rue");
        label_numero_rue_txt = new JLabel("label_numero_rue_txt");
        label_telephone_mobile = new JLabel("téléphone mobile");
        label_telephone_mobile_txt = new JLabel("label_telephone_mobile_txt");
        label_telephone_domicile = new JLabel("téléphone domicile");
        label_telephone_domicile_txt = new JLabel("label_telephone_domicile_txt");
        label_statut_matrimonial = new JLabel("statut matrimonial");
        label_statut_matrimonial_txt = new JLabel("label_statut_matrimonial_txt");
        label_nombre_enfants = new JLabel("nombre d'enfants");
        label_nombre_enfants_txt = new JLabel("label_nombre_enfants_txt");
        label_profession = new JLabel("profession");
        label_profession_txt = new JLabel("label_profession_txt");
        label_groupe_sanguin = new JLabel("groupe sanguin");
        label_groupe_sanguin_txt = new JLabel("label_groupe_sanguin_txt");
        label_substances_allergenes = new JLabel("substances allergènes");
        label_substances_allergenes_txt = new JLabel("label_substances_allergenes_txt");
        label_masse_corporelle = new JLabel("masse corporelle");
        label_masse_corporelle_txt = new JLabel("label_masse_corporelle_txt");
        label_statut_serologique = new JLabel("statut sérologique");
        label_statut_serologique_txt = new JLabel("label_statut_serologique_txt");
        label_glycemie = new JLabel("glycémie");
        label_glycemie_txt = new JLabel("label_glycemie_txt");
        bouton_close = new JButton("fermer");
        bouton_consultations = new JButton("voir consultations");
        
        this.getContentPane().setLayout(new BoxLayout(getContentPane(), BoxLayout.PAGE_AXIS));
        
        //*****panel_info_gene*****////
        panel_info_gene.setBorder(BorderFactory.createTitledBorder("Informations générales"));
        panel_info_gene.add(label_nom_patient);
        panel_info_gene.add(label_nom_patient_txt);
        panel_info_gene.add(label_prenom);
        panel_info_gene.add(label_prenom_txt);
        panel_info_gene.add(label_age);
        panel_info_gene.add(label_age_txt);
        panel_info_gene.add(label_sexe);
        panel_info_gene.add(label_sexe_txt);
        panel_info_gene.add(label_nationalite);
        panel_info_gene.add(label_nationalite_txt);
        panel_info_gene.add(label_taille);
        panel_info_gene.add(label_taille_txt);
        GridLayout grille = new GridLayout(3,4);
        panel_info_gene.setLayout(grille);
        this.getContentPane().add(panel_info_gene);
        //*****panel_info_gene*****////
        
        //*****panel_adresse*****////
        panel_adresse.setBorder(BorderFactory.createTitledBorder("adresse"));
        panel_adresse.add(label_pays);
        panel_adresse.add(label_pays_txt);
        panel_adresse.add(label_ville);
        panel_adresse.add(label_ville_txt);
        panel_adresse.add(label_numero_rue);
        panel_adresse.add(label_numero_rue_txt);
        panel_adresse.add(label_telephone_mobile);
        panel_adresse.add(label_telephone_mobile_txt);
        panel_adresse.add(label_telephone_domicile);
        panel_adresse.add(label_telephone_domicile_txt);
        panel_adresse.setLayout(grille);
        this.getContentPane().add(panel_adresse);
        //*****panel_adresse*****////
        
        //*****panel_civilité*****////
        panel_civilite.setBorder(BorderFactory.createTitledBorder("civilité"));
        panel_civilite.add(label_statut_matrimonial);
        panel_civilite.add(label_statut_matrimonial_txt);
        panel_civilite.add(label_nombre_enfants);
        panel_civilite.add(label_nombre_enfants_txt);
        panel_civilite.add(label_profession);
        panel_civilite.add(label_profession_txt);
        panel_civilite.setLayout(grille);
        this.getContentPane().add(panel_civilite);
        //*****panel_civilité*****////
        
        //*****panel_santé*****////
        panel_sante.setBorder(BorderFactory.createTitledBorder("santé"));
        panel_sante.add(label_groupe_sanguin);
        panel_sante.add(label_groupe_sanguin_txt);
        panel_sante.add(label_substances_allergenes);
        panel_sante.add(label_substances_allergenes_txt);
        panel_sante.add(label_masse_corporelle);
        panel_sante.add(label_masse_corporelle_txt);
        panel_sante.add(label_statut_serologique);
        panel_sante.add(label_statut_serologique_txt);
        panel_sante.add(label_glycemie);
        panel_sante.add(label_glycemie_txt);
        panel_sante.setLayout(grille);
        this.getContentPane().add(panel_sante);
        //*****panel_santé*****////
        
        panel_bouton.add(bouton_close);
        panel_bouton.add(bouton_consultations);
        bouton_close.addActionListener(this);
        bouton_consultations.addActionListener(this);
        this.getContentPane().add(panel_bouton);
        
        this.setTitle("Display informations"); //On donne un titre à l'application
        //permet d'afficher la fenetre en plein ecran
        this.setExtendedState(this.MAXIMIZED_BOTH); 
        //this.setResizable(false) ; //On interdit la redimensionnement de la fenêtre
	this.setDefaultCloseOperation(javax.swing.JFrame.EXIT_ON_CLOSE); //On dit à l'application de se fermer
		//lors du clic sur la croix
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        lbl_nom = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        lbl_nom.setText("Nom de la personne");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(167, 167, 167)
                .addComponent(lbl_nom)
                .addContainerGap(138, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(133, 133, 133)
                .addComponent(lbl_nom)
                .addContainerGap(153, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(DetailPatient.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(DetailPatient.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(DetailPatient.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(DetailPatient.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new DetailPatient().setVisible(true);
            }
        });
    }
    
    //methode permettant de modifier les valeurs des labels de la page de sorte à les remplacer par 
    //des valeurs prises en BD
    //il etait imperatif d'ajouter("throws SQLException") à cette methode pr utiliser ResulSet
    public void modifier_text_label(ResultSet result)throws SQLException{
        for(Component c : this.getContentPane().getComponents()){
                    if(c instanceof JPanel){
                        Component[] liste = ((JPanel) c).getComponents();
                        for(Component composant : liste){
                            if(((JLabel) composant).getText() == "label_nom_patient_txt")
                                ((JLabel) composant).setText(result.getString("nom_patient"));
                            if(((JLabel) composant).getText() == "label_prenom_txt")
                                ((JLabel) composant).setText(result.getString("prenom_patient"));
                            if(((JLabel) composant).getText() == "label_age_txt")
                                ((JLabel) composant).setText(result.getString("age_patient"));
                            if(((JLabel) composant).getText() == "label_sexe_txt")
                                ((JLabel) composant).setText(result.getString("sexe_patient"));
                            if(((JLabel) composant).getText() == "label_nationalite_txt")
                                ((JLabel) composant).setText(result.getString("nationalite_patient"));
                            if(((JLabel) composant).getText() == "label_taille_txt")
                                ((JLabel) composant).setText(result.getString("taille"));
                            if(((JLabel) composant).getText() == "label_pays_txt")
                                ((JLabel) composant).setText(result.getString("pays"));
                            if(((JLabel) composant).getText() == "label_ville_txt")
                                ((JLabel) composant).setText(result.getString("ville"));
                            if(((JLabel) composant).getText() == "label_numero_rue_txt")
                                ((JLabel) composant).setText(result.getString("numero_rue"));
                            if(((JLabel) composant).getText() == "label_telephone_mobile_txt")
                                ((JLabel) composant).setText(result.getString("telephone_mobile"));
                            if(((JLabel) composant).getText() == "label_telephone_domicile_txt")
                                ((JLabel) composant).setText(result.getString("telephone_domicile"));
                            if(((JLabel) composant).getText() == "label_statut_matrimonial_txt")
                                ((JLabel) composant).setText(result.getString("statut_matrimonial"));
                            if(((JLabel) composant).getText() == "label_nombre_enfants_txt")
                                ((JLabel) composant).setText(result.getString("nombre_enfants"));
                            if(((JLabel) composant).getText() == "label_profession_txt")
                                ((JLabel) composant).setText(result.getString("profession"));
                            if(((JLabel) composant).getText() == "label_groupe_sanguin_txt")
                                ((JLabel) composant).setText(result.getString("groupe_sanguin"));
                            if(((JLabel) composant).getText() == "label_substances_allergenes_txt")
                                ((JLabel) composant).setText(result.getString("substances_allergenes"));
                            if(((JLabel) composant).getText() == "label_masse_corporelle_txt")
                                ((JLabel) composant).setText(result.getString("masse_corporelle"));
                            if(((JLabel) composant).getText() == "label_statut_serologique_txt")
                                ((JLabel) composant).setText(result.getString("statut_serologique"));
                            if(((JLabel) composant).getText() == "label_glycemie_txt")
                                ((JLabel) composant).setText(result.getString("glycemie"));
                            
                        }
                    }
                }
    }
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel lbl_nom;
    // End of variables declaration//GEN-END:variables

    @Override
    public void actionPerformed(ActionEvent e) {
        if(e.getSource().equals(bouton_close)){
            DetailPatient.this.dispose();
            ListePatient listePatient = new ListePatient();
            listePatient.setVisible(true);
            
        }else if(e.getSource().equals(bouton_consultations)){
            //on libère la mémoire pour cette page
            DetailPatient.this.dispose();
            //on affiche la page listant les consultations dejà effectuées par le patient
            ConsultationPatientPage consultations = new ConsultationPatientPage();
            consultations.setVisible(true);
        }
    }
}
