/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package guinterface;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

/**
 *
 * @author GreenSoft Eng
 */
public class Enregistrement extends javax.swing.JFrame implements ActionListener{
    GridLayout grille = new GridLayout(2,2);
    Insets inset = new Insets(1,1,1,1);
    
    private boolean animated = true;
    
    private JPanel panel_global;
    private JPanel panel_identite;
    private JPanel panel_identifiants;
    private JPanel panel_boutons;
    
    //identite
    private JLabel nom;
    private JTextField nom_txt;
    private JLabel prenom;
    private JTextField prenom_txt;
    private JLabel age;
    private JTextField age_txt;
    private JLabel nationalite;
    private JComboBox nationalite_txt;
    //fin identite
    
    //identifiants
    private JLabel adresse_email;
    private JTextField adresse_email_txt;
    private JLabel login;
    private JTextField login_txt;
    private JLabel password;
    private JPasswordField  password_txt;
    private JLabel confirmation_password;
    private JPasswordField  confirmation_password_txt;
    //fin identifiants
    
    private JButton bouton_enregistrer;
    private JButton bouton_annuler;
    
    public Enregistrement(){
        
        panel_global = new JPanel();
        panel_global.setLayout(new BoxLayout(panel_global, BoxLayout.PAGE_AXIS));
        panel_identite = new JPanel();
        panel_identifiants = new JPanel();
        panel_boutons = new JPanel();
        
        //panel identite
        panel_identite.setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();
        //natural height, maximum width
        //c.fill = GridBagConstraints.HORIZONTAL;
        
        nom = new JLabel("nom");
        nom_txt = new JTextField();
        prenom = new JLabel("prénom");
        prenom_txt = new JTextField();
        age = new JLabel("age");
        age_txt = new JTextField();
        nationalite = new JLabel("nationalité");
        nationalite_txt = new JComboBox();
        
        c.gridx = 0;
        c.gridy = 0;
        c.gridheight = 1;
        c.gridwidth = 1;
        c.insets = inset;
        c.anchor = GridBagConstraints.FIRST_LINE_START;
        panel_identite.add(nom,c);
        
        c.gridx = 1;
        c.gridy = 0;
        c.gridheight = 1;
        c.gridwidth = 1;
        c.insets = inset;
        c.anchor = GridBagConstraints.FIRST_LINE_START;
        panel_identite.add(nom_txt,c);
        
        c.gridx = 2;
        c.gridy = 0;
        c.gridheight = 1;
        c.gridwidth = 1;
        c.insets = inset;
        c.anchor = GridBagConstraints.PAGE_START;
        panel_identite.add(prenom,c);
        
        c.gridx = 3;
        c.gridy = 0;
        c.gridheight = GridBagConstraints.REMAINDER;
        c.gridwidth = GridBagConstraints.REMAINDER;
        c.insets = inset;
        c.anchor = GridBagConstraints.PAGE_START;
        panel_identite.add(prenom_txt,c);
        
        c.gridx = 0;
        c.gridy = 1;
        c.gridheight = 1;
        c.gridwidth = 1;
        c.insets = inset;
        panel_identite.add(age,c);
        
        c.gridx = 1;
        c.gridy = 1;
        c.gridheight = 1;
        c.gridwidth = 1;
        c.insets = inset;
        panel_identite.add(age_txt,c);
        
        c.gridx = 2;
        c.gridy = 1;
        c.gridheight = 1;
        c.gridwidth = 1;
        c.insets = inset;
        panel_identite.add(nationalite,c);
        
        c.gridx = 3;
        c.gridy = 1;
        c.gridheight = GridBagConstraints.REMAINDER;
        c.gridwidth = GridBagConstraints.REMAINDER;
        c.insets = inset;
        panel_identite.add(nationalite_txt,c);
        
        panel_identite.setBorder(BorderFactory.createTitledBorder("IDENTITE"));
        //fin panel identite
        
        //panel identifiants
        GridLayout grille2 = new GridLayout(4,1);
        panel_identifiants.setLayout(grille2);
        adresse_email = new JLabel("adresse email");
        adresse_email_txt = new JTextField();
        login = new JLabel("login");
        login_txt = new JTextField();
        password = new JLabel("password");
        password_txt = new JPasswordField ();
        confirmation_password = new JLabel("confirmation password");
        confirmation_password_txt = new JPasswordField ();
        
        panel_identifiants.add(adresse_email);
        panel_identifiants.add(adresse_email_txt);
        panel_identifiants.add(login);
        panel_identifiants.add(login_txt);
        panel_identifiants.add(password);
        panel_identifiants.add(password_txt);
        panel_identifiants.add(confirmation_password);
        panel_identifiants.add(confirmation_password_txt);
        
        panel_identifiants.setBorder(BorderFactory.createTitledBorder("IDENTIFICATION"));
        //fin panel identifiants
        
        bouton_enregistrer = new JButton("ENREGISTRER");
        bouton_annuler = new JButton("ANNULER");
        
        //panel boutons
        panel_boutons.add(bouton_enregistrer);
        panel_boutons.add(bouton_annuler);
        //fin panel
        
        //panel global
        panel_global.add(panel_identite);
        panel_global.add(panel_identifiants);
        panel_global.add(panel_boutons);
        
        //fin panel
        
        this.getContentPane().add(panel_global);
        this.setTitle("ENREGISTREMENT");
        this.setMinimumSize(new Dimension(200,24));
        //permet d'afficher la fenetre en plein ecran
        this.setExtendedState(this.MAXIMIZED_BOTH); 
        this.setDefaultCloseOperation(javax.swing.JFrame.EXIT_ON_CLOSE); //On dit à l'application de se fermer
		//lors du clic sur la croix
                
        bouton_enregistrer.addActionListener(this);
    }
    
    public void actionPerformed(ActionEvent e) {
        System.out.println("tout doux");
        JOptionPane jop = new JOptionPane();    	
      int option = jop.showConfirmDialog(null, 
        "Voulez-vous lancer l'animation ?", 
        "Lancement de l'animation", 
        JOptionPane.YES_NO_OPTION, 
        JOptionPane.QUESTION_MESSAGE);

      if(option == JOptionPane.OK_OPTION){
        System.out.println("tout doux");
      }
    }
    
}
