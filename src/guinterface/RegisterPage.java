/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package guinterface;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

/**
 *
 * @author GreenSoft Eng
 */
public class RegisterPage extends javax.swing.JFrame implements ActionListener{
    
    private JPanel panel_global,panel_identite,panel_boutons;
    
    //identite
    private JLabel nom,prenom,sexe,telephone,pays,adresse_email,numero_ordre,centre_medical,password,confirmation_password;
    private JTextField nom_txt,prenom_txt,telephone_txt,adresse_email_txt,numero_ordre_txt,centre_medical_txt,password_txt,confirmation_password_txt;
    private JComboBox sexe_txt;
    private JComboBox pays_txt;
//fin identite
    
    private JButton bouton_enregistrer;
    private JButton bouton_annuler;
    
    public RegisterPage(){
        this.getContentPane().setLayout(new BoxLayout(getContentPane(), BoxLayout.PAGE_AXIS));
        
//        panel_global = new JPanel();
//        panel_global.setLayout(new BoxLayout(panel_global, BoxLayout.PAGE_AXIS));
        panel_identite = new JPanel();
        panel_boutons = new JPanel();
        
        bouton_enregistrer = new JButton("Enregistrer");
        bouton_annuler = new JButton("annuler");
        
        //on dit ici que le click sur le bouton est ecouté par l'objet fenetre(RegisterPage)        
        bouton_enregistrer.addActionListener(this);
        //on dit ici que le click sur le bouton est ecouté par l'objet fenetre(RegisterPage)        
        bouton_annuler.addActionListener(this);
        
        //panel_boutons.setBorder(BorderFactory.createTitledBorder("Identifiants"));
        panel_boutons.add(bouton_enregistrer);
        panel_boutons.add(bouton_annuler);
        
        //panel identite
        panel_identite.setBorder(BorderFactory.createTitledBorder("Enregistrement Medecin"));
        panel_identite.setLayout(null);
        
        nom = new JLabel("Nom");
        nom.setBounds(400, 50, 100, 30);
        nom_txt = new JTextField();
        nom_txt.setBounds(700, 50, 250, 35);
        prenom = new JLabel("Prenom");
        prenom.setBounds(400, 100, 250, 35);
        prenom_txt = new JTextField();
        prenom_txt.setBounds(700, 100, 250, 35);
        sexe = new JLabel("Sexe");
        sexe.setBounds(400, 150, 250, 35);
        sexe_txt = new JComboBox();
        sexe_txt.setBounds(700, 150, 250, 35);
        sexe_txt.addItem("fais un choix");
        sexe_txt.addItem("Masculin");
        sexe_txt.addItem("Féminin");
        telephone = new JLabel("Télephone");
        telephone.setBounds(400, 200, 250, 35);
        telephone_txt = new JTextField();
        telephone_txt.setBounds(700, 200, 250, 35);
        pays = new JLabel("pays");
        pays.setBounds(400, 250, 250, 35);
        pays_txt = new JComboBox();
        pays_txt.setBounds(700, 250, 250, 35);
        
        pays_txt.addItem("choisis un pays");
        pays_txt.addItem("Cameroun");
        pays_txt.addItem("Chine");
        pays_txt.addItem("Congo");
        pays_txt.addItem("Gabon");
        pays_txt.addItem("Guinéé équatoriale");
        pays_txt.addItem("Kénya");
        pays_txt.addItem("Tchad");
        
        adresse_email = new JLabel("email");
        adresse_email.setBounds(400, 300, 250, 35);
        adresse_email_txt = new JTextField();
        adresse_email_txt.setBounds(700, 300, 250, 35);
        numero_ordre = new JLabel("numero d'ordre");
        numero_ordre.setBounds(400, 350, 250, 35);
        numero_ordre_txt = new JTextField();
        numero_ordre_txt.setBounds(700, 350, 250, 35);
        centre_medical = new JLabel("Centre Médical");
        centre_medical.setBounds(400, 400, 250, 35);
        centre_medical_txt = new JTextField();
        centre_medical_txt.setBounds(700, 400, 250, 35);
        password = new JLabel("password");
        password.setBounds(400, 450, 250, 35);
        password_txt = new JPasswordField();
        password_txt.setBounds(700, 450, 250, 35);
        confirmation_password = new JLabel("confirmation password");
        confirmation_password.setBounds(400, 500, 250, 35);
        confirmation_password_txt = new JPasswordField();
        confirmation_password_txt.setBounds(700, 500, 250, 35);
        
        
        bouton_enregistrer.setBounds(500, 600, 150, 35);
        bouton_annuler.setBounds(670, 600, 150, 35);
        
        
        panel_identite.add(adresse_email);
        panel_identite.add(adresse_email_txt);
        panel_identite.add(numero_ordre);
        panel_identite.add(numero_ordre_txt);
        panel_identite.add(centre_medical);
        panel_identite.add(centre_medical_txt);
        panel_identite.add(password);
        panel_identite.add(password_txt);
        panel_identite.add(confirmation_password);
        panel_identite.add(confirmation_password_txt);
        
        panel_identite.add(nom);
        panel_identite.add(nom_txt);
        panel_identite.add(prenom);
        panel_identite.add(prenom_txt);
        panel_identite.add(sexe);
        panel_identite.add(sexe_txt);
        panel_identite.add(telephone);
        panel_identite.add(telephone_txt);
        panel_identite.add(pays);
        panel_identite.add(pays_txt);
        panel_identite.add(bouton_enregistrer);
        panel_identite.add(bouton_annuler);
        //fin panel identite
        
        //this.getContentPane().add(panel_global);
        this.setTitle("ENREGISTREMENT");
        this.setMinimumSize(new Dimension(200,24));
        //permet d'afficher la fenetre en plein ecran
        this.setExtendedState(this.MAXIMIZED_BOTH); 
        this.setDefaultCloseOperation(javax.swing.JFrame.EXIT_ON_CLOSE); //On dit à l'application de se fermer
		//lors du clic sur la croix
                
        
        this.getContentPane().add(panel_identite);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if(e.getSource().equals(bouton_enregistrer)){
            
            //on verifie que tous les champs sont remplis et respectent les contraintes
            if(nom_txt.getText().compareTo("") == 0 || prenom_txt.getText().compareTo("") == 0 || sexe_txt.getSelectedItem().toString().compareTo("fais un choix") == 0 || telephone_txt.getText().compareTo("") == 0 || pays_txt.getSelectedItem().toString().compareTo("choisis un pays") == 0 || adresse_email_txt.getText().compareTo("") == 0 
                    || numero_ordre_txt.getText().compareTo("") == 0 || centre_medical_txt.getText().compareTo("") == 0 || password_txt.getText().compareTo("") == 0 || confirmation_password_txt.getText().compareTo("") == 0){
                
                JOptionPane dialog = new JOptionPane();
                dialog.showMessageDialog(null, "un ou plusieurs champs sont vides", "alerte", JOptionPane.ERROR_MESSAGE);
               
            }
            else{
                if(confirmation_password_txt.getText().compareTo(password_txt.getText()) == 0){
                    JOptionPane dialog = new JOptionPane();
                    dialog.showMessageDialog(null, "Bravo", "alerte", JOptionPane.ERROR_MESSAGE);
                
                }else{
                    JOptionPane dialog = new JOptionPane();
                    dialog.showMessageDialog(null, "les mots de passe ne sont pas identiques", "alerte", JOptionPane.ERROR_MESSAGE);
                }
                
                //on enregistre en Base de données
            
            }
            
        }else if(e.getSource().equals(bouton_annuler)){
            
            JOptionPane jop = new JOptionPane();    	
            int option = jop.showConfirmDialog(null, 
                "Voulez-vous vraiment annuler l'opération en cours ?", 
                "Annulation", 
                JOptionPane.YES_NO_OPTION, 
                JOptionPane.QUESTION_MESSAGE);

            if(option == JOptionPane.OK_OPTION){
                //on libère la mémoire pour cette page
                RegisterPage.this.dispose();
                //on affiche la page de Login
                LoginPage loginPage = new LoginPage();
                loginPage.setVisible(true);
            }
            if(option == JOptionPane.NO_OPTION){
                
            }
            
        }
        
    }
}
