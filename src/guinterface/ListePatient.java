/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package guinterface;

import java.awt.Dimension;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;

/**
 *
 * @author GreenSoft Eng
 */

//CTRL + SHIFT + O pour générer les imports
public class ListePatient extends JFrame {

  public ListePatient(){

    //Les données du tableau
    Object[][] data = {
      {"Cysboy", "luc", "11-10-1997"},
      {"BZHHydde", "antoine", "23-12-1995"},
      {"IamBow", "girèse", "02-05-1976"},
      {"FunMan", "alain", "12-09-1987"}
    };

    //Les titres des colonnes
    String  title[] = {"Nom", "Prenom", "Date de Naissance"};
    JTable tableau = new JTable(data, title);
    //Nous ajoutons notre tableau à notre contentPane dans un scroll
    //Sinon les titres des colonnes ne s'afficheront pas !
    this.getContentPane().add(new JScrollPane(tableau));
    
    this.setTitle("Liste des Patients");
    this.setMinimumSize(new Dimension(200,24));
    //permet d'afficher la fenetre en plein ecran
    this.setExtendedState(this.MAXIMIZED_BOTH); 
    this.setDefaultCloseOperation(javax.swing.JFrame.EXIT_ON_CLOSE); //On dit à l'application de se fermer
		//lors du clic sur la croix
  }   
}