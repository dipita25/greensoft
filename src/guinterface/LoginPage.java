/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package guinterface;

import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.font.TextAttribute;
import java.util.Map;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

/**
 *
 * @author GreenSoft Eng
 */
public class LoginPage extends javax.swing.JFrame implements ActionListener{
    
    private JLabel login,password,inscription;
    private JTextField login_txt;
    private JPasswordField  password_txt;
    private JPanel panel_global,panel_inscription,panel_bloc_login,panel_bloc_password,panel_zone_identification;
    private JButton bouton_connect;
    private JOptionPane dialog;
    
    public LoginPage(){
        
        login = new JLabel("login");
        password = new JLabel("password");
        inscription = new JLabel("pas de compte? CREER UN COMPTE ICI");
        login_txt = new JTextField();
        password_txt = new JPasswordField ();
        bouton_connect = new JButton("se connecter");
        panel_global = new JPanel();
        panel_inscription = new JPanel();
        panel_bloc_login = new JPanel();
        panel_bloc_password = new JPanel();
        panel_zone_identification = new JPanel();
        
        panel_bloc_login.add(login);
        panel_bloc_login.add(login_txt);
        panel_inscription.add(inscription);
        Dimension d = new Dimension(900, 900);
        login_txt.setPreferredSize( new Dimension( 200, 24 ) );
        password_txt.setPreferredSize( new Dimension( 200, 24 ) );
        panel_bloc_password.add(password);
        panel_bloc_password.add(password_txt);
        panel_bloc_password.add(panel_inscription);
        
        //zone contenant les donnees affichees; les elements se placent les uns sur les autres
        panel_zone_identification.setLayout(new BoxLayout(panel_zone_identification, BoxLayout.PAGE_AXIS));
        panel_zone_identification.add(panel_bloc_login);
        panel_zone_identification.add(panel_bloc_password);
        panel_zone_identification.add(bouton_connect);
        panel_zone_identification.setMaximumSize(new Dimension(100, 100));
        
        //bloc permettant de souligner le texte et modifier sa police du label indiquant le lien d'inscription
        Font font = inscription.getFont();
        Map attributes = font.getAttributes();
        attributes.put(TextAttribute.UNDERLINE, TextAttribute.UNDERLINE_ON);
        inscription.setFont(font.deriveFont(attributes));
        //inscription.setFont(new Font("Serif", Font.PLAIN, 14));
        inscription.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        //fin bloc
        
        //this.getContentPane().setLayout(new BoxLayout(getContentPane(), BoxLayout.PAGE_AXIS));
        this.getContentPane().add(panel_zone_identification);
        this.setTitle("Identification"); //On donne un titre à l'application
        this.setSize(500,300); //On donne une taille à notre fenêtre
	this.setLocationRelativeTo(null); //On centre la fenêtre sur l'écran
        this.setResizable(false) ; //On interdit la redimensionnement de la fenêtre
	this.setDefaultCloseOperation(javax.swing.JFrame.EXIT_ON_CLOSE); //On dit à l'application de se fermer
		//lors du clic sur la croix
                
        //on dit ici que le click sur le bouton est ecouté par l'objet fenetre(detailPatient)        
        bouton_connect.addActionListener(this);
        
        //action qui se déclenche lors du clic sur ce label indiquant aller s'inscrire
        inscription.addMouseListener(new MouseAdapter()  
        {  
            public void mouseClicked(MouseEvent e)  
            {  
               LoginPage.this.dispose();
                 RegisterPage registerPage = new RegisterPage();
                 registerPage.setVisible(true);
            }  
        }); 
    }
    
    //Méthode qui sera appelée lors d'un clic sur le bouton
    public void actionPerformed(ActionEvent arg0) {
        
        if((login_txt.getText().toString().compareTo("dipita25") == 0) & (password_txt.getText().toString().compareTo("flature25") == 0)){
            //on épure la fenetre et toutes les ressources associées
            this.dispose();
            
            //on appelle la fenetre suivante
            ListePatient listePatientPage = new ListePatient();
            listePatientPage.setVisible(true);
        }
        else{
            System.out.println("données incorrectes");
            dialog = new JOptionPane();
            dialog.showMessageDialog(null, "login ou password incorrect", "alerte", JOptionPane.ERROR_MESSAGE);
        }
    } 
    
}
